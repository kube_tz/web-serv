FROM nginx

COPY entrypoint.sh /

COPY app /usr/share/nginx/html

RUN usermod -u 1001 nginx && groupmod -g 1001 nginx

CMD ["./entrypoint.sh"]
